package org.microservices.customer.web;

import lombok.AllArgsConstructor;
import org.microservices.customer.entities.Customer;
import org.microservices.customer.services.CustomerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    @PostMapping("customers")
    public void saveCustomer(@RequestBody Customer customerRequest){
        Customer customer = Customer.builder()
                .name(customerRequest.getName())
                .email(customerRequest.getEmail())
                .build();
        customerService.saveCustomer(customer);
    }
}
