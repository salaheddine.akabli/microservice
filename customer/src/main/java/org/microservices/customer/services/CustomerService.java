package org.microservices.customer.services;

import lombok.AllArgsConstructor;
import org.microservices.customer.entities.Customer;
import org.microservices.customer.repositories.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional
@AllArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final RestTemplate restTemplate;

    public Customer saveCustomer(Customer customer){
        String url = "https://FRUD/check-fraud";
        Object respense = restTemplate.getForObject(url,Customer.class,customer.getId());
        return customerRepository.save(customer);

    }

}
