package org.microservices.order.util;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class ProductRest extends RestTemplate {

    public Map<String, Boolean> isInStock(){

        return this.exchange("PRODUCT-SERVICE",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Map<String, Boolean>>() {
        }).getBody();
    }


}
