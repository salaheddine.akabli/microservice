package org.microservices.order.mappers;

import org.microservices.order.dto.OrderLineItemsDTO;
import org.microservices.order.dto.OrderRequest;
import org.microservices.order.dto.OrderRespense;
import org.microservices.order.entities.Order;
import org.microservices.order.entities.OrderLineItems;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class OrderMapper {


    public Order fromOrderRequest(OrderRequest orderRequest){
        return Order.builder()
                .orderLineItemsList(
                        orderRequest.getOrderLineItemsList().stream()
                                .map(this::fromDTO)
                                .collect(Collectors.toList())
                )
                .build();
    }

    public OrderRespense toOrderRespense(Order order){
        return OrderRespense.builder()
                .id(order.getId())
                .orderNumber(order.getOrderNumber())
                .orderLineItemsList(order.getOrderLineItemsList())
                .build();
    }

    public OrderLineItemsDTO toDTO(OrderLineItems orderLineItems){
        return OrderLineItemsDTO.builder()
                .id(orderLineItems.getId())
                .skuCode(orderLineItems.getSkuCode())
                .price(orderLineItems.getPrice())
                .quantity(orderLineItems.getQuantity())
                .build();
    }

    public OrderLineItems fromDTO(OrderLineItemsDTO orderLineItemsDTO){
        return OrderLineItems.builder()
                .id(orderLineItemsDTO.getId())
                .skuCode(orderLineItemsDTO.getSkuCode())
                .price(orderLineItemsDTO.getPrice())
                .quantity(orderLineItemsDTO.getQuantity())
                .build();
    }
}
