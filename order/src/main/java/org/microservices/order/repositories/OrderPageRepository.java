package org.microservices.order.repositories;

import org.microservices.order.entities.Order;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderPageRepository extends PagingAndSortingRepository<Order, Long> {

}
