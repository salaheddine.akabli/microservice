package org.microservices.order.dto;

import jakarta.persistence.CascadeType;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.microservices.order.entities.OrderLineItems;

import java.util.List;
@NoArgsConstructor @AllArgsConstructor @Data @Builder
public class OrderRespense {

    private Long id;
    private String orderNumber;
    private List<OrderLineItems> orderLineItemsList;
}
