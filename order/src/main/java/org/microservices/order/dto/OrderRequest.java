package org.microservices.order.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.microservices.order.entities.OrderLineItems;

import java.util.List;

@NoArgsConstructor @AllArgsConstructor @Data @Builder
public class OrderRequest {
    private List<OrderLineItemsDTO> orderLineItemsList;
}
