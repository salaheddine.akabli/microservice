package org.microservices.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(name = "INVENTORY-SERVICE")
public interface InventoryFeign {

    @GetMapping("/inventorys")
    //@Headers(value = "Content-Type: application/json")
    Map<String, Boolean> isInStock(@RequestParam(name = "code") List<String> items);
}
