package org.microservices.order.web;

import lombok.AllArgsConstructor;
import org.microservices.order.dto.OrderRequest;
import org.microservices.order.dto.OrderRespense;
import org.microservices.order.services.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
@AllArgsConstructor
public class OrderController {

    private final OrderService orderService;
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public OrderRespense saveOrder(@RequestBody OrderRequest orderRequest){
        OrderRespense order = orderService.saveOrder(orderRequest);
        return order;
    }

    @PostMapping("/savefile")
    public String saveFile(@RequestBody() MultipartFile file){

        String str = file.getOriginalFilename();
        String type = file.getContentType();
        String name = file.getName();
        long size = file.getSize();
        System.out.println("Full name : "+str);
        System.out.println("Type : "+type);
        System.out.println("Name : "+ name);
        System.out.println("Size : "+ size);
        return "file saved succesfuly";
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<OrderRespense> getAll(){
        return orderService.getAll();
    }
}
