package org.microservices.order;

import org.microservices.order.filter.OrderFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    FilterRegistrationBean<OrderFilter> orderFilterFilterRegistrationBean(){

        FilterRegistrationBean<OrderFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new OrderFilter());
        registrationBean.addUrlPatterns("/order/**");
        return registrationBean;
    }
}
