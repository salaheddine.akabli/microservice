package org.microservices.order.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.microservices.order.dto.OrderLineItemsDTO;
import org.microservices.order.dto.OrderRequest;
import org.microservices.order.dto.OrderRespense;
import org.microservices.order.entities.Order;
import org.microservices.order.feign.InventoryFeign;
import org.microservices.order.mappers.OrderMapper;
import org.microservices.order.repositories.OrderPageRepository;
import org.microservices.order.repositories.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class OrderService {


    private final OrderMapper orderMapper;
    private final OrderRepository orderRepository;
    private final InventoryFeign inventoryFeign;
    private final OrderPageRepository orderPageRepository;
    public OrderRespense saveOrder(OrderRequest orderRequest){
        List<String> items = orderRequest.getOrderLineItemsList().stream()
                .map(OrderLineItemsDTO::getSkuCode).toList();
        Map<String, Boolean> stocks = inventoryFeign.isInStock(items);
        if (stocks.isEmpty()){
            System.out.println("empty");
        }else {
            stocks.entrySet().forEach(e-> System.out.println(e.getKey()+" : "+e.getValue()));
        }

        long count = stocks.entrySet().stream()
                .filter(e -> !e.getValue()).count();
        System.out.println(count);
        if (count > 0){
            System.out.println(count +"items dosn`t exist in our stock for now");
            return null;
        }else{
            Order order = orderMapper.fromOrderRequest(orderRequest);
            order.setOrderNumber(UUID.randomUUID().toString());
            orderRepository.save(order);
            log.info("order {} saved", order.getOrderNumber());
            return orderMapper.toOrderRespense(order);
        }


    }

    public List<OrderRespense> getAll() {
        List<Order> orders = orderRepository.findAll();
        return orders.stream()
                .map(orderMapper::toOrderRespense)
                .toList();
    }

    public Page<Order> getAll(Pageable pageable){
        return orderPageRepository.findAll(pageable);
    }

}
