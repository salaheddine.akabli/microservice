package org.microservices.product.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor @AllArgsConstructor @Data @Builder
public class ProductRequest {
    @NotBlank()
    @Size(min = 8,max = 30, message = "Name's size should be between 8 and 30 character")
    private String name;
    private String description;
    @NotNull(message = "Price should be not null")
    private BigDecimal price;
}
