package org.microservices.product.web;

import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import lombok.AllArgsConstructor;
import org.microservices.product.dto.ProductRequest;
import org.microservices.product.dto.ProductRespense;
import org.microservices.product.exceptions.ProductNotFoundException;
import org.microservices.product.services.ProdcutService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.List;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProducController {

    private final ProdcutService prodcutService;
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<ProductRespense> getProducts(){
        List<ProductRespense> products = prodcutService.getAllProduct();
        return products;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductRespense getProductById(@PathParam("id") String productId) throws ProductNotFoundException {
        return prodcutService.getProduct(productId);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.OK)
    public ProductRespense saveProduct(@Valid @RequestBody ProductRequest productRequest){
        return prodcutService.saveProduct(productRequest);
    }
}
