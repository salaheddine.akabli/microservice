package org.microservices.product.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.microservices.product.dto.ProductRequest;
import org.microservices.product.dto.ProductRespense;
import org.microservices.product.entities.Product;
import org.microservices.product.exceptions.ProductNotFoundException;
import org.microservices.product.mappers.ProductMapper;
import org.microservices.product.repositories.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProdcutService{
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
 
    @Override
    public ProductRespense saveProduct(ProductRequest productRequest) {
        Product product = productMapper.RequesttoProduct(productRequest);
        product.setId(UUID.randomUUID().toString());
        productRepository.save(product);
        log.info("Product {} saved", product.getId());
        return productMapper.toProductRespense(product);
    }

    @Override
    public List<ProductRespense> getAllProduct() {
        List<Product> products = productRepository.findAll();
        return products.stream()
                .map(productMapper::toProductRespense)
                .collect(Collectors.toList());
    }

    @Override
    public ProductRespense getProduct(String productId) throws ProductNotFoundException{
        Product product = productRepository.findById(productId)
                .orElseThrow(
                        () -> new ProductNotFoundException("Product not found !")
                );
        return productMapper.toProductRespense(product);
    }
}
