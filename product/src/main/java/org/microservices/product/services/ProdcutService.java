package org.microservices.product.services;

import org.microservices.product.dto.ProductRequest;
import org.microservices.product.dto.ProductRespense;
import org.microservices.product.exceptions.ProductNotFoundException;

import java.util.List;

public interface ProdcutService {

    ProductRespense saveProduct(ProductRequest productRequest);
    List<ProductRespense> getAllProduct();
    ProductRespense getProduct(String productId) throws ProductNotFoundException;
}
