package org.microservices.product.mappers;

import org.microservices.product.dto.ProductRequest;
import org.microservices.product.dto.ProductRespense;
import org.microservices.product.entities.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    public Product RequesttoProduct(ProductRequest productRequest){
        return Product.builder()
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .build();
    }

    public ProductRequest toProductRequest(Product product){
        return ProductRequest.builder()
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .build();
    }

    public ProductRespense toProductRespense(Product product){
        return ProductRespense.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .build();
    }
    public Product respenseToProduct(ProductRespense productRespense){
        return Product.builder()
                .id(productRespense.getId())
                .name(productRespense.getName())
                .description(productRespense.getDescription())
                .price(productRespense.getPrice())
                .build();
    }
}
