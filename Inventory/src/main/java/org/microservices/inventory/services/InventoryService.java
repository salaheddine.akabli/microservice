package org.microservices.inventory.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.microservices.inventory.dto.InventoryRespense;
import org.microservices.inventory.entities.Inventory;
import org.microservices.inventory.mappers.InventoryMapper;
import org.microservices.inventory.repositories.InventoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class InventoryService {

    private final InventoryRepository inventoryRepository;
    private final InventoryMapper inventoryMapper;

    public Boolean isInStuck(String skuCode){
        /*
        Map<String, Boolean> stock = new HashMap<>();
        skuCode.stream().map(code -> stock.put(code,inventoryRepository.existsBySkuCode(code)));
         */
        return inventoryRepository.existsBySkuCode(skuCode);
    }
    public Map<String, Boolean>  isInStuck(List<String> skuCode){
        Map<String, Boolean> stock = new HashMap<>();
        skuCode.forEach(code -> stock.put(code,inventoryRepository.existsBySkuCode(code)));
        return stock;
    }

    public List<InventoryRespense> findAll() {
        List<Inventory> inventories = inventoryRepository.findAll();
        return inventories.stream()
                    .map(inventoryMapper::toInventoryRespense)
                    .collect(Collectors.toList());
    }
}
