package org.microservices.inventory.mappers;

import org.microservices.inventory.dto.InventoryRequest;
import org.microservices.inventory.dto.InventoryRespense;
import org.microservices.inventory.entities.Inventory;
import org.springframework.stereotype.Component;

@Component
public class InventoryMapper {

    public Inventory fromRequest(InventoryRequest request){
        return Inventory.builder()
                .skuCode(request.getSkuCode())
                .quantity(request.getQuantity())
                .build();
    }

    public InventoryRespense toInventoryRespense(Inventory inventory){
        return InventoryRespense.builder()
                .id(inventory.getId())
                .skuCode(inventory.getSkuCode())
                .quantity(inventory.getQuantity())
                .build();
    }
}
