package org.microservices.inventory.dto;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data  @AllArgsConstructor @NoArgsConstructor @Builder
public class InventoryRequest {
    private String skuCode;
    private Integer quantity;
}
