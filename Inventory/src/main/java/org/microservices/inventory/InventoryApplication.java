package org.microservices.inventory;

import org.microservices.inventory.entities.Inventory;
import org.microservices.inventory.repositories.InventoryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class InventoryApplication {
    public static void main(String[] args) {
        SpringApplication.run(InventoryApplication.class, args);
    }


    CommandLineRunner run(InventoryRepository inventoryRepository){
        return args -> {
            inventoryRepository.save(
                    Inventory.builder()
                            .quantity(3)
                            .skuCode("Iphone 8")
                            .build()
            );
            inventoryRepository.save(
                    Inventory.builder()
                            .quantity(4)
                            .skuCode("Iphone Xr")
                            .build()
            );
        };
    }
}
