package org.microservices.inventory.web;

import lombok.AllArgsConstructor;
import org.microservices.inventory.dto.InventoryRespense;
import org.microservices.inventory.services.InventoryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@AllArgsConstructor
public class InventoryController {

    private final InventoryService inventoryService;
    @GetMapping("/inventorys")
    public Map<String, Boolean> isInStock(@RequestParam("code") List<String> skucode){
        System.out.println(skucode);
        Map<String, Boolean> stock = inventoryService.isInStuck(skucode);
        if (stock.isEmpty()){
            System.out.println("empty");
        }else{
            stock.entrySet()
                    .forEach(e->System.out.println(e.getKey() + " : " + e.getValue()));
        }
        return stock;
    }

    @GetMapping("/allinventory")
    @ResponseStatus(HttpStatus.OK)
    public List<InventoryRespense> getAll(){
        return inventoryService.findAll();
    }
}
